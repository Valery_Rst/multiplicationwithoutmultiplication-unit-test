import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
/**
 *Юнит-тест реализует проверку метода multiplication класса MultiplicationWithoutMultiplication для доказательства корректности кода.
 *Метод assertEquals означает, что ожидаемый результат и полученный результат совпадают .
 */

class MultiplicationWithoutMultiplicationTest {
    private static int firstValue, secondValue;

    @Test
    void multiplicationWithPositiveValue() {

        firstValue = 11;
        secondValue = 11;
        assertEquals(firstValue * secondValue, MultiplicationWithoutMultiplication.multiplication(firstValue, secondValue));
    }

    @Test
    void multiplicationWithNegative() {
        firstValue = -11;
        secondValue = -11;
        assertEquals(firstValue * secondValue, MultiplicationWithoutMultiplication.multiplication(firstValue, secondValue));
    }

    @Test
    void multiplicationWithNegativeFirstValue () {
        firstValue = -11;
        secondValue = 11;
        assertEquals(firstValue * secondValue, MultiplicationWithoutMultiplication.multiplication(firstValue, secondValue));
    }

    @Test
    void multiplicationWithNegativeSecondValue() {
        firstValue = 11;
        secondValue = -11;
        assertEquals(firstValue * secondValue, MultiplicationWithoutMultiplication.multiplication(firstValue, secondValue));
    }

    @Test
    void multiplicationWithZero() {
        firstValue = 0;
        secondValue = 0;
        assertEquals(firstValue * secondValue, MultiplicationWithoutMultiplication.multiplication(firstValue, secondValue));
    }

    @Test
    void multiplicationWithSecondValueZero() {
        firstValue = 11;
        secondValue = 0;
        assertEquals(firstValue * secondValue, MultiplicationWithoutMultiplication.multiplication(firstValue, secondValue));
    }
}