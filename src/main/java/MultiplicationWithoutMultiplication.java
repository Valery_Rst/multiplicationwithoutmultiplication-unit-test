import java.util.Scanner;

public class MultiplicationWithoutMultiplication {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите первое значение: ");
        int firstValue = scanner.nextInt();
        System.out.print("Введите второе значение: ");
        int secondValue = scanner.nextInt();

        System.out.printf("Произведенгие выражения %d * %d = %d", firstValue,secondValue, multiplication(firstValue, secondValue));
    }

    /**
     * Метод производит умножение двух значений, введённых пользователем, с помощью алгоритма накопления суммы.
     * @param firstValue- первое значение, введённое пользователем.
     * @param secondValue- второе значение, введённое пользователем.
     * @return result- результат выполнения метода
     */
    static int multiplication(int firstValue, int secondValue) {
        /*
          В случае если один или оба значения являются нулём, возвращается ноль.
         */
        if (firstValue == 0 || secondValue == 0) return 0;

        /*
         * В случае если оба значения отрицательны, производится замена знака на противоположный.
         */
        if (firstValue < 0 & secondValue < 0) {
            firstValue *= -1;
            secondValue *= -1;
        }

        /*
         * В случае если второе значение отрицательно, производится замена знака на противоположный. В противном случае результат будет некорректен.
         */
        if (secondValue < 0) {
            firstValue *= -1;
            secondValue *= -1;
        }

        int result = 0;
        for (int i = 1; i <= secondValue; i++){
            result += firstValue;
        }
        return result;
    }
}